package com.junior.itspartner

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent



class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcom)

        val logoTimer = object : Thread() {
            override fun run() {
                try {
                    Thread.sleep(5000)
                    startActivity(Intent(applicationContext, MainActivity::class.java))
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } finally {
                    finish()
                }
            }
        }
        logoTimer.start()

    }
}
