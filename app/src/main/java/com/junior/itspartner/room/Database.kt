package com.junior.itspartner.room


import android.arch.persistence.room.RoomDatabase

import com.junior.itspartner.room.data.Training

@android.arch.persistence.room.Database(entities = arrayOf(Training::class), version = 1)
abstract class Database : RoomDatabase() {
    abstract fun trainingDAO(): TrainingDAO
}