package com.junior.itspartner.room


import android.arch.persistence.room.*

import com.junior.itspartner.room.data.Training

@Dao
interface TrainingDAO {

    @get:Query("SELECT * FROM training")
    val all: List<Training>

    @Query("SELECT * FROM training WHERE id = :arg0")
    fun getById(id: Long): Training

    @Insert
    fun insert(training: Training)

    @Update
    fun update(training: Training)

    @Delete
    fun delete(training: Training)

}
