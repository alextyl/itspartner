package com.junior.itspartner.room.data


import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class Training {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    var date: Long? = null
    var comment: String? = null
    var count: Int = 0

    override fun toString(): String {
        return id.toString() + " " + date.toString() + " " + comment + " " + count.toString()
    }
}