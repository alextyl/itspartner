package com.junior.itspartner.fragments

import android.app.Fragment
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.junior.itspartner.Application.App
import com.junior.itspartner.R
import com.junior.itspartner.SaveTraining
import com.junior.itspartner.dialogs.CommentDialog
import com.junior.itspartner.room.data.Training


class TrainingFragment : Fragment(), SensorEventListener{

    private var startEnd: Button? = null
    private var sensor: Sensor? = null
    private var sensorManager: SensorManager? = null
    private var fragmentContext: Context? = null
    private var countOfTurns: TextView? = null
    private var imageViewLeft: ImageView? = null
    private var imageViewRight: ImageView? = null

    private var rightTurn: Boolean = false
    private var leftTurn: Boolean = false
    private var newTurn: Boolean = true

    private var save: SaveTraining? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fragmentContext = context
        save = context as SaveTraining
    }

    override fun onDetach() {
        super.onDetach()
        if(startEnd!!.text == getString(R.string.end)) endTraining()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        sensorManager = fragmentContext?.getSystemService(Context.SENSOR_SERVICE) as SensorManager?
        sensor = sensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

        val v = inflater!!.inflate(R.layout.fragment_training, container, false)
        startEnd = v.findViewById(R.id.startEnd)
        countOfTurns = v.findViewById(R.id.countOfTurns)
        imageViewLeft = v.findViewById(R.id.imageViewLeft)
        imageViewRight = v.findViewById(R.id.imageViewRight)

        startEnd!!.setOnClickListener(View.OnClickListener {
            if (startEnd!!.text == getString(R.string.start)){
               startEnd!!.setText(R.string.end)
               sensorManager?.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
               imageViewLeft!!.setImageResource(R.drawable.cross)
               imageViewRight!!.setImageResource(R.drawable.cross)
               imageViewLeft!!.visibility = View.VISIBLE
               imageViewRight!!.visibility = View.VISIBLE
            } else {
                startEnd!!.setText(R.string.start)
                sensorManager?.unregisterListener(this)
                imageViewLeft!!.visibility = View.GONE
                imageViewRight!!.visibility = View.GONE
                save!!.save(countOfTurns!!.text.toString().toInt())
                countOfTurns!!.text = "0"
            } })

        return v
    }

    override fun onPause() {
        super.onPause()
        sensorManager?.unregisterListener(this)
    }

    override fun onResume() {
        super.onResume()
        if(startEnd!!.text == getString(R.string.end))
        sensorManager?.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }

    override fun onSensorChanged(p0: SensorEvent?) {
        if(p0!!.sensor.type == Sensor.TYPE_ACCELEROMETER){
            val degreeX = p0.values[0].toInt()
            if(degreeX == 0) newTurn = true
            if(newTurn){
                if(degreeX > 9){
                    leftTurn = true
                    imageViewLeft!!.setImageResource(R.drawable.mark)
                } else if (degreeX < -9){
                    rightTurn = true
                    imageViewRight!!.setImageResource(R.drawable.mark)
                }
                if(leftTurn && rightTurn){
                    leftTurn = false
                    rightTurn = false
                    newTurn = false
                    imageViewLeft!!.setImageResource(R.drawable.cross)
                    imageViewRight!!.setImageResource(R.drawable.cross)
                    countOfTurns!!.text = (countOfTurns!!.text.toString().toInt() + 1).toString()
                }
            }
        }
    }

    private fun endTraining(comment: String = "comment"){
        val db = App.instance.database
        val trainingDao = db.trainingDAO()
        val training = Training()
        training.count = countOfTurns!!.text.toString().toInt()
        training.date = System.currentTimeMillis() / 1000
        training.comment = comment
        Thread(Runnable {
            trainingDao.insert(training)
        }).start()

    }

}