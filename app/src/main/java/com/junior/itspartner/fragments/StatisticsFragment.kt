package com.junior.itspartner.fragments

import android.annotation.SuppressLint
import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.junior.itspartner.Application.App
import com.junior.itspartner.R
import android.support.annotation.Nullable
import android.widget.ArrayAdapter
import android.widget.TextView
import com.junior.itspartner.room.data.Training
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList



class StatisticsFragment : Fragment() {

    private var listView: ListView? = null
    private var trainings: ArrayList<Training> = ArrayList()
    private var fragmentContext: Context? = null
    private var handler: Handler? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val db = App.instance.database
        val trainingDao = db.trainingDAO()

        val v = inflater!!.inflate(R.layout.fragment_statistics, container, false)
        listView = v.findViewById(R.id.listView)

        Thread(Runnable {
            trainings.addAll(trainingDao.all)
            handler!!.sendEmptyMessage(0)
        }).start()

        handler = Handler(Handler.Callback {
            setAdapter()
            true
        })

        return v
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fragmentContext = context
    }

    private inner class PhotoListAdapter(context: Context, dataListItems: ArrayList<Training>) : ArrayAdapter<Training>(context, 0, dataListItems) {

        @SuppressLint("SimpleDateFormat")
        override fun getView(position: Int, @Nullable convertView: View?, parent: ViewGroup): View {
            var convertView = convertView

            val dataListItem = getItem(position)

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false)
            }

            val count: TextView = convertView!!.findViewById(R.id.count)
            val comment: TextView = convertView.findViewById(R.id.comment)
            val date: TextView = convertView.findViewById(R.id.time)

            count.text = dataListItem!!.count.toString()
            comment.text = dataListItem.comment
            date.text = SimpleDateFormat("MM/dd/yyyy").format(Date(dataListItem.date!! * 1000))

            return convertView
        }
    }

    private fun setAdapter() {
        listView?.adapter = PhotoListAdapter(fragmentContext!!, trainings)

    }


}