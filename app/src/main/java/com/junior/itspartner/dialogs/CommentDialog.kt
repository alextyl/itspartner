package com.junior.itspartner.dialogs


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.EditText
import android.widget.TextView
import com.junior.itspartner.Application.App
import com.junior.itspartner.MainActivity

import com.junior.itspartner.R
import com.junior.itspartner.room.data.Training

class CommentDialog : DialogFragment() {



    @SuppressLint("SetTextI18n")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)

        val inflater = activity.layoutInflater

        val v = inflater.inflate(R.layout.dialog_comment, null)

        val textView = v.findViewById<TextView>(R.id.countOfTurnsNumber)
        textView.text = textView.text.toString() + " " + MainActivity.count

        builder.setView(v)
                .setPositiveButton("OK") { dialog, id ->
                    val editText = v.findViewById<EditText>(R.id.editText)
                    val db = App.instance.database
                    val trainingDao = db.trainingDAO()
                    val training = Training()
                    training.count = MainActivity.count
                    training.date = System.currentTimeMillis() / 1000
                    training.comment = editText.text.toString()
                    Thread(Runnable {
                        trainingDao.insert(training)
                    }).start()}
        return builder.create()

    }
}
