package com.junior.itspartner

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.junior.itspartner.dialogs.CommentDialog
import com.junior.itspartner.fragments.StatisticsFragment
import com.junior.itspartner.fragments.TrainingFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), SaveTraining {

    companion object {
         var count: Int = 0
    }

    override fun save(count: Int) {
        MainActivity.count = count
        CommentDialog().show(supportFragmentManager, "commit")
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_training -> {
                fragmentManager.beginTransaction().replace(R.id.fragment, TrainingFragment()).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_statistics -> {
                fragmentManager.beginTransaction().replace(R.id.fragment, StatisticsFragment()).commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fragmentManager.beginTransaction().replace(R.id.fragment, TrainingFragment()).commit()
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

}
